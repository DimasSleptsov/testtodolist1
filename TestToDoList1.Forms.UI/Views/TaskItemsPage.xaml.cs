﻿using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestToDoList1.Core.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestToDoList1.Forms.UI.Views
{
    [MvxTabbedPagePresentation(TabbedPosition.Tab, WrapInNavigationPage = true, Icon ="task_items_icon.png")]
    public partial class TaskItemsPage : MvxContentPage<TaskItemsViewModel>
    {
        public TaskItemsPage()
        {
            InitializeComponent();
        }
    }
}