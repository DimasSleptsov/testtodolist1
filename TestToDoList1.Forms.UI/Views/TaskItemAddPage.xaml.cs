﻿using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestToDoList1.Core.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestToDoList1.Forms.UI.Views
{
    [MvxNavigationPagePresentation(WrapInNavigationPage = true, NoHistory = false)]
    public partial class TaskItemAddPage : MvxContentPage<TaskItemAddViewModel>
    {
        public TaskItemAddPage()
        {
            InitializeComponent();
        }
    }
}