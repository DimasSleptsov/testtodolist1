﻿using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestToDoList1.Core.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestToDoList1.Forms.UI.Views
{
    [MvxContentPagePresentation(WrapInNavigationPage = false, NoHistory = true)]
    public partial class MainPage : MvxContentPage<MainViewModel>
    {
        public MainPage()
        {

            InitializeComponent();

        }

        protected override void OnAppearing()
        {
            ViewModel.ShowViewPagerCommand.Execute();
            ViewModel.CloseMainViewCommand.Execute();
            base.OnAppearing();
        }
    }
}