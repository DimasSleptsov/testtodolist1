﻿using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestToDoList1.Core.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestToDoList1.Forms.UI.Views
{
    [MvxTabbedPagePresentation(TabbedPosition.Root, WrapInNavigationPage = true, NoHistory = true)]
    public partial class TabbedPage : MvxTabbedPage<ViewPagerViewModel>
    {
        private bool _firstTime = true;

        public TabbedPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (_firstTime)
            {
                ViewModel.ShowTaskItemsCommand.Execute();
                ViewModel.ShowUserDescriptionCommand.Execute();
                
                _firstTime = false;
            }

            base.OnAppearing();
        }
    }
}