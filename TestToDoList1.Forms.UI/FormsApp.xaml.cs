﻿using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestToDoList1.Forms.UI
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FormsApp : Application
	{
		public FormsApp ()
		{
			InitializeComponent ();
		}
	}
}