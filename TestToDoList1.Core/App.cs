﻿using MvvmCross.IoC;
using MvvmCross.ViewModels;
using TestToDoList1.Core.ViewModels;

namespace TestToDoList1.Core
{
    public class App : MvxApplication
    {
        public const string DataBaseName = "TestToDoList.db";

        public override void Initialize()
        {
            base.Initialize();

            CreatableTypes()
                .EndingWith("Repository")
                .AsInterfaces()
                .RegisterAsLazySingleton();


            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();


            RegisterCustomAppStart<AppStart>();
        }
    }
}
