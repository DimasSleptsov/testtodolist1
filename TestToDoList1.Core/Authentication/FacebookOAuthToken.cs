﻿namespace TestToDoList1.Core.Authentication
{
    public class FacebookOAuthToken
    {
        public string AccessToken { get; set; }
    }
}
