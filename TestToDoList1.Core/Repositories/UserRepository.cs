﻿using MvvmCross;
using SQLite;
using TestToDoList1.Core.Models.Entities;
using TestToDoList1.Core.Repositories.Interfaces;
using TestToDoList1.Core.Services;

namespace TestToDoList1.Core.Repositories
{
    public class UserRepository : IUserRepository
    {
        public SQLiteConnection Connection { get; set; }

        public UserRepository()
        {
            Connection = new SQLiteConnection(Mvx.IoCProvider.Resolve<ISQLiteService>().GetName());
            Connection.CreateTable<UserEntity>();
        }

        public void Add(UserEntity data)
        {
            Connection.Insert(data);
        }

        public UserEntity Get(long id)
        {
            return Connection.Find<UserEntity>(id);
        }
    }
}
