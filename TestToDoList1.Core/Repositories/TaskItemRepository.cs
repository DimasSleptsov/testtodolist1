﻿using MvvmCross;
using SQLite;
using System.Collections.Generic;
using TestToDoList1.Core.Models.Entities;
using TestToDoList1.Core.Repositories.Interfaces;
using TestToDoList1.Core.Services;

namespace TestToDoList1.Core.Repositories
{
    public class TaskItemRepository : ITaskItemRepository
    {
        public List<TaskItemEntity> GetUserTasks(long userId)
        {
            List<TaskItemEntity> userItems = Connection.Query<TaskItemEntity>(string.Format("SELECT * FROM[TaskItems] WHERE UserId = {0}", userId));
            return userItems;
        }

        public SQLiteConnection Connection { get; set; }
        public TaskItemRepository()
        {
            Connection = new SQLiteConnection(Mvx.IoCProvider.Resolve<ISQLiteService>().GetName());
            Connection.CreateTable<TaskItemEntity>();
        }

        public void Add(TaskItemEntity data)
        {
            Connection.Insert(data);
        }
        public void Update(TaskItemEntity data)
        {
            Connection.Update(data);
        }
        public IEnumerable<TaskItemEntity> GetAll()
        {
            return Connection.Table<TaskItemEntity>().ToList();
        }
        public TaskItemEntity Get(int id)
        {
            return Connection.Find<TaskItemEntity>(id);
        }
        public void Delete(int id)
        {
            Connection.Delete<TaskItemEntity>(id);
        }
    }
}
