﻿using TestToDoList1.Core.Models.Entities;

namespace TestToDoList1.Core.Repositories.Interfaces
{
    public interface IUserRepository
    {
        void Add(UserEntity data);
        UserEntity Get(long id);
    }
}
