﻿using System.Collections.Generic;
using TestToDoList1.Core.Models.Entities;

namespace TestToDoList1.Core.Repositories.Interfaces
{
    public interface ITaskItemRepository
    {
        List<TaskItemEntity> GetUserTasks(long userId);
        void Add(TaskItemEntity data);
        void Update(TaskItemEntity data);
        IEnumerable<TaskItemEntity> GetAll();
        TaskItemEntity Get(int id);
        void Delete(int id);
    }
}
