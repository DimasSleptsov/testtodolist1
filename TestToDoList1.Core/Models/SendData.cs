﻿using System;
using System.Collections.Generic;
using System.Text;
using TestToDoList1.Core.Enums;
using TestToDoList1.Core.Models.Entities;

namespace TestToDoList1.Core.Models
{
    public class SendData
    {
        public TaskItemEntity SendEntity { get; set; }
        public SendType SendType { get; set; }
    }
}
