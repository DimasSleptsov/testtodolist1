﻿using SQLite;

namespace TestToDoList1.Core.Models.Entities
{
    [Table("Users")]
    public class UserEntity
    {
        [PrimaryKey]
        public long Id { get; set; }
    }
}
