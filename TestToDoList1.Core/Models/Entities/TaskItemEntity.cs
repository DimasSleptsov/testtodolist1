﻿using SQLite;

namespace TestToDoList1.Core.Models.Entities
{
    [Table("TaskItems")]
    public class TaskItemEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDone { get; set; }
        public long UserId { get; set; }
    }
    
}
