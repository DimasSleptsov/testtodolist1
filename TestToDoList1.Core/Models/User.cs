﻿using Newtonsoft.Json;

namespace TestToDoList1.Core.Models
{
    public class User
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public Picture Picture { get; set; }
    }
    public class Picture
    {
        public PictureData Data;
    }
    public class PictureData
    {
        public int Height { get; set; }
        public int Width { get; set; }
        [JsonProperty("is_silhouette")]
        public bool IsSilhouette { get; set; }
        public string Url { get; set; }
    }

}
