﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestToDoList1.Core.Enums
{
    public enum SendType
    {
        Delete,
        Update
    }
}
