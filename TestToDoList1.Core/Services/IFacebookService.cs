﻿using System.Threading.Tasks;
using TestToDoList1.Core.Models;

namespace TestToDoList1.Core.Services
{
    public interface IFacebookService
    {
        Task<User> SetCurrentUser(string accessToken);
        User GetCurrentUser();
    }
}
