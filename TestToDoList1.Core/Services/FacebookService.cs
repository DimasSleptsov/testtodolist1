﻿using Newtonsoft.Json;
using Plugin.SecureStorage;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using TestToDoList1.Core.Models;

namespace TestToDoList1.Core.Services
{
    public class FacebookService:IFacebookService
    {
        public async Task<User> SetCurrentUser(string accessToken)
        {
            HttpClient httpClient = new HttpClient();
            string json = await httpClient.GetStringAsync($"https://graph.facebook.com/me?fields=id,email,name,picture&access_token={accessToken}");
            User user = JsonConvert.DeserializeObject<User>(json);
            CrossSecureStorage.Current.SetValue(nameof(User), json);
            return user;
        }
        public User GetCurrentUser()
        {
            try
            {
                string jsonObj = CrossSecureStorage.Current.GetValue(nameof(User));
                User result = JsonConvert.DeserializeObject<User>(jsonObj);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
