﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Auth;

namespace TestToDoList1.Core.Services
{
    public interface IFacebookUIService
    {
        void GetFacebookUI(OAuth2Authenticator auth);
    }
}
