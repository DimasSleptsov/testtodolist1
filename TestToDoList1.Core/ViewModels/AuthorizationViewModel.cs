﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using System;
using TestToDoList1.Core.Authentication;
using TestToDoList1.Core.Models;
using TestToDoList1.Core.Models.Entities;
using TestToDoList1.Core.Repositories.Interfaces;
using TestToDoList1.Core.Services;
using Xamarin.Auth;

namespace TestToDoList1.Core.ViewModels
{
    public class AuthorizationViewModel : BaseViewModel
    {
        #region Variables

        private IUserRepository _userRepository;
        private IFacebookService _facebookService;
        private IFacebookUIService _facebookUIService;
        private User _user;

        #endregion

        #region Constructors 

        public AuthorizationViewModel(IMvxNavigationService navigationService, IFacebookService facebookService, IUserRepository userRepository, IFacebookUIService facebookUIService)
            : base(navigationService)
        {
            _facebookUIService = facebookUIService;
            _userRepository = userRepository;
            _facebookService = facebookService;
        }

        #endregion

        #region Properties

        public OAuth2Authenticator Authenticator { get; set; }

        #endregion

        #region Commands

        public MvxCommand FacebookLoginCommand => new MvxCommand(() => { FacebookLogin(); });

        #endregion

        #region Methods

        private void FacebookLogin()
        {
            Authenticator = new OAuth2Authenticator(
                clientId: "245100766394216",
                scope: "email",
                authorizeUrl: new Uri("https://www.facebook.com/v2.10/dialog/oauth/"),
                redirectUrl: new Uri("https://www.facebook.com/connect/login_success.html"));
            Authenticator.Completed += OnFacebookAuthCompleted;
            Authenticator.AllowCancel = true;
            Authenticator.Error += (sender, eventArgs) =>
            {
                OAuth2Authenticator auth2 = (OAuth2Authenticator)sender;
                auth2.ShowErrors = false;
                auth2.OnCancelled();
            };

            _facebookUIService.GetFacebookUI(Authenticator);
        }

        private async void OnFacebookAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            if (e.IsAuthenticated)
            {
                FacebookOAuthToken token = new FacebookOAuthToken
                {
                    AccessToken = e.Account.Properties["access_token"]
                };

                _user = await _facebookService.SetCurrentUser(token.AccessToken);

                if (_userRepository.Get(_user.Id) == null)
                {
                    UserEntity user = new UserEntity
                    {
                        Id = _user.Id
                    };
                    _userRepository.Add(user);
                }
                await NavigationService.Close(this);
                await NavigationService.Navigate<MainViewModel>();
                return;
            }
            await NavigationService.Close(this);
            await NavigationService.Navigate<AuthorizationViewModel>();
        }

        #endregion
    }
}