﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using Plugin.SecureStorage;
using TestToDoList1.Core.Models;
using TestToDoList1.Core.Services;

namespace TestToDoList1.Core.ViewModels
{
    public class UserDescriptionViewModel : BaseViewModel
    {
        #region Variables

        private readonly IFacebookService _facebookService;

        #endregion

        #region Constructors

        public UserDescriptionViewModel(IMvxNavigationService navigationService, IFacebookService facebookService)
            : base(navigationService)
        {
            _facebookService = facebookService;
            User user = facebookService.GetCurrentUser();
            Email = user.Email;
            Name = user.Name;
            PictureURL = user.Picture.Data.Url;
        }

        #endregion

        #region Properties

        public string Email { get; set; }
        public string Name { get; set; }
        public string PictureURL { get; set; }

        #endregion

        #region Commands

        public MvxCommand LogOutUserCommand => new MvxCommand(() => { LogOutUser(); });

        #endregion

        #region Methods

        private void LogOutUser()
        {
            CrossSecureStorage.Current.DeleteKey(nameof(User));
            NavigationService.Close(this);
            NavigationService.Navigate<AuthorizationViewModel>();
            
        }

        #endregion
    }
}