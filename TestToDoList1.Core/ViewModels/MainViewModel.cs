﻿using MvvmCross.Commands;
using MvvmCross.Navigation;

namespace TestToDoList1.Core.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        #region Constructors

        public MainViewModel(IMvxNavigationService navigationService)
            : base(navigationService)
        {
        }

        #endregion

        #region Commands

        public MvxAsyncCommand ShowTaskItemAddCommand => new MvxAsyncCommand(async () => { await NavigationService.Navigate<TaskItemAddViewModel>(); });

        public MvxAsyncCommand ShowViewPagerCommand => new MvxAsyncCommand(async () => { await NavigationService.Navigate<ViewPagerViewModel>(); });

        public MvxAsyncCommand CloseMainViewCommand => new MvxAsyncCommand(async () => { await NavigationService.Close(this); });

        #endregion
    }
}