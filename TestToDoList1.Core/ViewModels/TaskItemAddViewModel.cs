﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Plugin.Messenger;
using System;
using TestToDoList1.Core.Models.Entities;
using TestToDoList1.Core.Repositories.Interfaces;
using TestToDoList1.Core.Services;

namespace TestToDoList1.Core.ViewModels
{
    public class TaskItemAddViewModel : BaseViewModel<bool,TaskItemEntity>
    {
        #region Variables

        private readonly ITaskItemRepository _taskItemRepository;
        private IFacebookService _facebookService;

        private string _errorName;
        private string _errorDescription;
        private string _name;
        private string _description;
        private bool _isDone;

        #endregion

        #region Constructors

        public TaskItemAddViewModel(IMvxNavigationService navigationService, ITaskItemRepository taskItemRepository, IFacebookService facebookService) 
            : base(navigationService)
        {
            _taskItemRepository = taskItemRepository;
            _facebookService = facebookService;

        }

        #endregion

        #region Properties

        public string ErrorName
        {
            get
            {
                return _errorName;
            }
            set
            {
                _errorName = value;
                RaisePropertyChanged(() => ErrorName);
            }
        }

        public string ErrorDescription
        {
            get
            {
                return _errorDescription;
            }
            set
            {
                _errorDescription = value;
                RaisePropertyChanged(() => ErrorDescription);
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }
      
        public bool IsDone
        {
            get
            {
                return _isDone;
            }
            set
            {
                _isDone = value;
                RaisePropertyChanged(() => IsDone);
            }
        }

        #endregion

        #region Commands

        public MvxCommand AddEntityCommand => new MvxCommand(() => { AddEntity(); });

        #endregion

        #region Methods

        public void AddEntity()
        {
            if (Validate())
            {
                TaskItemEntity Entity = new TaskItemEntity
                {
                    Name = this.Name.Trim(),
                    Description = this.Description.Trim(),
                    IsDone = this.IsDone,
                    UserId = _facebookService.GetCurrentUser().Id
                };
                _taskItemRepository.Add(Entity);
                NavigationService.Close(this, Entity);
            }
        }

        public override void Prepare(bool parameter)
        {
        }

        public bool Validate()
        {
            bool isValidate = true;
            if (String.IsNullOrWhiteSpace(this.Name))
            {
                ErrorName = "Write task name!";
                isValidate = false;
            }
            if (String.IsNullOrWhiteSpace(this.Description))
            {
                ErrorDescription = "Write task description!";
                isValidate = false;
            }
            return isValidate;
        }

        #endregion
    }
}
