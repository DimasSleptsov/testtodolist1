﻿using System.Threading.Tasks;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace TestToDoList1.Core.ViewModels
{
    public class BaseViewModel : MvxViewModel
    {
        public IMvxNavigationService NavigationService { get; protected set; }

        public BaseViewModel(IMvxNavigationService mvxNavigationService)
        {
            NavigationService = mvxNavigationService;
        }
    }

    public abstract class BaseViewModel<TParameter> : BaseViewModel, IMvxViewModel<TParameter>
    {
        protected BaseViewModel(IMvxNavigationService navigationService) 
            : base(navigationService)
        {
        }

        public abstract void Prepare(TParameter parameter);
    }

    public abstract class BaseViewModel<TParametr, TResult> : BaseViewModel, IMvxViewModel<TParametr, TResult>
    {
        protected BaseViewModel(IMvxNavigationService navigationService)
            : base(navigationService)
        {
        }

        public abstract void Prepare(TParametr parameter);

        public TaskCompletionSource<object> CloseCompletionSource { get; set; }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            if (viewFinishing && CloseCompletionSource != null && !CloseCompletionSource.Task.IsCompleted && !CloseCompletionSource.Task.IsFaulted)
                CloseCompletionSource?.TrySetCanceled();

            base.ViewDestroy(viewFinishing);
        }
    }
}
