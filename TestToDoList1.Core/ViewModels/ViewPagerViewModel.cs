﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Plugin.Messenger;
using TestToDoList1.Core.Repositories.Interfaces;
using TestToDoList1.Core.Services;

namespace TestToDoList1.Core.ViewModels
{
    public class ViewPagerViewModel : BaseViewModel
    {
        #region Constructors

        public ViewPagerViewModel(IMvxNavigationService navigationService, ITaskItemRepository taskItemRepository, IFacebookService facebookService, IMvxMessenger messenger)
            : base(navigationService)
        {
            TaskItemsViewModel = new TaskItemsViewModel(navigationService, taskItemRepository, facebookService);
            UserDescriptionViewModel = new UserDescriptionViewModel(navigationService, facebookService);
        }

        #endregion

        #region Properties

        public TaskItemsViewModel TaskItemsViewModel { get; set; }
        public UserDescriptionViewModel UserDescriptionViewModel { get; set; }

        #endregion

        #region Commands

        public IMvxAsyncCommand ShowTaskItemsCommand => new MvxAsyncCommand(async () => { await NavigationService.Navigate<TaskItemsViewModel>(); });

        public IMvxAsyncCommand ShowUserDescriptionCommand => new MvxAsyncCommand(async () => { await NavigationService.Navigate<UserDescriptionViewModel>(); });

        #endregion
    }
}
