﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using TestToDoList1.Core.Enums;
using TestToDoList1.Core.Models;
using TestToDoList1.Core.Models.Entities;
using TestToDoList1.Core.Repositories.Interfaces;
using TestToDoList1.Core.Services;

namespace TestToDoList1.Core.ViewModels
{
    public class TaskItemsViewModel : BaseViewModel
    {
        #region Variables

        private readonly ITaskItemRepository _taskItemRepository;
        private IFacebookService _facebookService;

        private bool _isRefreshing;
        private MvxObservableCollection<TaskItem> _taskItems;

        #endregion

        #region Constructors

        public TaskItemsViewModel(IMvxNavigationService mvxNavigationService, ITaskItemRepository taskItemRepository, IFacebookService facebookService)
            : base(mvxNavigationService)
        {
            AutoMapper.Mapper.Reset();
            Mapper.Initialize(cfg => cfg.CreateMap<TaskItemEntity, TaskItem>());

            TaskItems = new MvxObservableCollection<TaskItem>();
            
            TaskItemSelectedCommand = new MvxAsyncCommand<TaskItem>(TaskItemSelected);

            _taskItemRepository = taskItemRepository;
            _facebookService = facebookService;
        }

        #endregion

        #region Properties

        public MvxObservableCollection<TaskItem> TaskItems
        {
            get { return _taskItems; }
            set
            {
                _taskItems = value;
                RaisePropertyChanged(() => TaskItems);
            }
        }


        public virtual bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                RaisePropertyChanged(() => IsRefreshing);
            }
        }

        #endregion

        #region Lifecycles

        public override void ViewAppearing()
        {
            base.ViewAppearing();
        }
        
        public override void ViewCreated()
        {
            base.ViewCreated();
            RefreshTaskItems();
        }

        #endregion

        #region Commands

        public MvxCommand RefreshTaskItemsCommand => new MvxCommand(() =>
        {
            IsRefreshing = true;
            RefreshTaskItems();
            IsRefreshing = false;
        });

        public IMvxAsyncCommand ShowTaskItemAddCommand => new MvxAsyncCommand(async () => { await TaskItemAdd(); });

        public IMvxCommand<TaskItem> TaskItemSelectedCommand { get; private set; }

        #endregion

        #region Methods

        private async Task TaskItemSelected(TaskItem selectedTaskItem)
        {
            SendData result = await NavigationService.Navigate<TaskDescriptionViewModel, TaskItem, SendData>(selectedTaskItem);
            if (result == null)
            {
                return;
            }

            if (result.SendType == SendType.Update)
            {
                TaskItem taskItem = Mapper.Map<TaskItemEntity, TaskItem>(result.SendEntity);
                int index = _taskItems.IndexOf(selectedTaskItem);
                _taskItems.RemoveAt(index);
                _taskItems.Insert(index, taskItem);
                return;
            }

            if (result.SendType == SendType.Delete)
            {
                _taskItems.Remove(selectedTaskItem);
            }
        }

        public void RefreshTaskItems()
        {
            TaskItems.Clear();
            long userId = _facebookService.GetCurrentUser().Id;

            var userItems = _taskItemRepository.GetUserTasks(userId);
            TaskItems.AddRange(userItems.Select(v => {
                TaskItem taskItem = Mapper.Map<TaskItemEntity, TaskItem>(v);
                return taskItem;
            }));
        }

        private async Task TaskItemAdd()
        {
            TaskItemEntity result = await NavigationService.Navigate<TaskItemAddViewModel, bool, TaskItemEntity>(true);
            if (result == null)
            {
                return;
            }
            TaskItem taskItem = Mapper.Map<TaskItemEntity, TaskItem>(result);
            _taskItems.Add(taskItem);
        }

        #endregion
    }
}
