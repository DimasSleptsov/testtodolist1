﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using System;
using TestToDoList1.Core.Enums;
using TestToDoList1.Core.Models;
using TestToDoList1.Core.Models.Entities;
using TestToDoList1.Core.Repositories.Interfaces;
using TestToDoList1.Core.Services;

namespace TestToDoList1.Core.ViewModels
{
    public class TaskDescriptionViewModel : BaseViewModel<TaskItem, SendData>
    {
        #region Variables

        private readonly ITaskItemRepository _taskItemRepository;
        private IFacebookService _facebookService;
        private TaskItem _taskItem;
        private IShareFacebookService _shareFacebookService;

        private string _errorDescription;
        private string _name;
        private string _description;
        private bool _isDone;

        #endregion

        #region Constructors

        public TaskDescriptionViewModel(IMvxNavigationService NavigationService, ITaskItemRepository taskItemRepository, IFacebookService facebookService, IShareFacebookService shareFacebookService) 
            : base(NavigationService)
        {
            _facebookService = facebookService;
            _taskItemRepository = taskItemRepository;
            _shareFacebookService = shareFacebookService;
        }

        #endregion

        #region Properties

        public string Title { get; set; }

        public string ErrorDescription
        {
            get
            {
                return _errorDescription;
            }
            set
            {
                _errorDescription = value;
                RaisePropertyChanged(() => ErrorDescription);
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }

        public bool IsDone
        {
            get
            {
                return _isDone;
            }
            set
            {
                _isDone = value;
                RaisePropertyChanged(() => IsDone);
            }
        }

        #endregion

        #region Commands

        public MvxCommand ShareFacebookCommand => new MvxCommand(() => { _shareFacebookService.Share(); });

        public MvxCommand DeleteEntityCommand => new MvxCommand(() => { DeleteEntity(); });

        public MvxCommand UpdateEntityCommand => new MvxCommand(() => { UpdateEntity(); });

        #endregion

        #region Override

        public override void Prepare(TaskItem parameter)
        {
            _taskItem = parameter;
            Title = _taskItem.Name;
            Name = _taskItem.Name;
            Description = _taskItem.Description;
            IsDone = _taskItem.IsDone;
        }

        #endregion

        #region Methods

        public void UpdateEntity()
        {
            if (Validate())
            {
                TaskItemEntity Entity = new TaskItemEntity
                {
                    Id = _taskItem.Id,
                    Name = this.Name,
                    Description = this.Description.Trim(),
                    IsDone = this.IsDone,
                    UserId = _facebookService.GetCurrentUser().Id
                };
                _taskItemRepository.Update(Entity);
                NavigationService.Close(this, new SendData { SendEntity = Entity,SendType = SendType.Update });
            }
        }
        public void DeleteEntity()
        {
            _taskItemRepository.Delete(_taskItem.Id);
            NavigationService.Close(this, new SendData {SendType = SendType.Delete});
        }

        public bool Validate()
        {
            bool isValidate = true;
            if (String.IsNullOrWhiteSpace(this.Description))
            {
                ErrorDescription = "Write task description!";
                isValidate = false;
            }
            return isValidate;
        }

        #endregion
    }
}