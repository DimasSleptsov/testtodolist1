﻿using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Threading.Tasks;
using TestToDoList1.Core.Models;
using TestToDoList1.Core.Services;
using TestToDoList1.Core.ViewModels;

namespace TestToDoList1.Core
{
    public class AppStart : MvxAppStart
    {
        private IMvxNavigationService _navigationService;
        private IFacebookService _facebookService;

        public AppStart(IMvxApplication app, IMvxNavigationService mvxNavigationService, IFacebookService facebookService)
            : base(app, mvxNavigationService)
        {
            _navigationService = mvxNavigationService;
            _facebookService = facebookService;
        }

        protected override async Task NavigateToFirstViewModel(object hint = null)
        {
            User user = _facebookService.GetCurrentUser();
            if (user != null)
            {
                await _navigationService.Navigate<MainViewModel>();
                return;
            }
            await _navigationService.Navigate<AuthorizationViewModel>();
        }
    }
}
