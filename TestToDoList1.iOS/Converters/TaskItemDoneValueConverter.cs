﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace TestToDoList1.iOS.Converters
{
    public class TaskItemDoneValueConverter : MvxValueConverter<bool,bool>
    {
        protected override bool Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ? false : true;
        }
    }
}