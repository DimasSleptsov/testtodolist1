﻿using MvvmCross.Platforms.Ios.Core;
using TestToDoList1.Core;
using MvvmCross.ViewModels;
using MvvmCross;
using TestToDoList1.Core.Services;
using TestToDoList1.iOS.Services;

namespace TestToDoList1.iOS
{
    public class Setup : MvxIosSetup<App>
    {
        protected override IMvxApplication CreateApp()
        {
            Mvx.IoCProvider.RegisterSingleton<ISQLiteService>(new SQLiteService());
            Mvx.IoCProvider.RegisterSingleton<IShareFacebookService>(new ShareFacebookService());
            Mvx.IoCProvider.RegisterSingleton<IFacebookUIService>(new FacebookUIService());
            return new App();
        }

    }
}