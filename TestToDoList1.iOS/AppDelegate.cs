﻿using Foundation;
using MvvmCross.Platforms.Ios.Core;
using TestToDoList1.Core;
using UIKit;

namespace TestToDoList1.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : MvxApplicationDelegate<Setup, App>
    {
        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            base.FinishedLaunching(application, launchOptions);
            return true;
        }

        protected override void RunAppStart(object hint = null)
        {
            base.RunAppStart(hint);
        }
    }
}
