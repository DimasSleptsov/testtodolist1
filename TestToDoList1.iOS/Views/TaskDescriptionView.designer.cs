// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [Register ("TaskDescriptionView")]
    partial class TaskDescriptionView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton DeleteButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SaveButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton shareFacebookButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView TaskItemDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch TaskItemDone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TaskItemName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DeleteButton != null) {
                DeleteButton.Dispose ();
                DeleteButton = null;
            }

            if (SaveButton != null) {
                SaveButton.Dispose ();
                SaveButton = null;
            }

            if (shareFacebookButton != null) {
                shareFacebookButton.Dispose ();
                shareFacebookButton = null;
            }

            if (TaskItemDescription != null) {
                TaskItemDescription.Dispose ();
                TaskItemDescription = null;
            }

            if (TaskItemDone != null) {
                TaskItemDone.Dispose ();
                TaskItemDone = null;
            }

            if (TaskItemName != null) {
                TaskItemName.Dispose ();
                TaskItemName = null;
            }
        }
    }
}