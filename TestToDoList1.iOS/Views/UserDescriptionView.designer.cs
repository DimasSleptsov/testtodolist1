// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [Register ("UserDescriptionView")]
    partial class UserDescriptionView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton logOutButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel userEmail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        FFImageLoading.Cross.MvxCachedImageView userImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel userName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (logOutButton != null) {
                logOutButton.Dispose ();
                logOutButton = null;
            }

            if (userEmail != null) {
                userEmail.Dispose ();
                userEmail = null;
            }

            if (userImage != null) {
                userImage.Dispose ();
                userImage = null;
            }

            if (userName != null) {
                userName.Dispose ();
                userName = null;
            }
        }
    }
}