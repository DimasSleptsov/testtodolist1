using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Views;
using System;
using TestToDoList1.Core.Models;
using UIKit;

namespace TestToDoList1.iOS.Views.Cells
{
    [MvxFromStoryboard("Main")]
    public partial class TaskItemsCellView : MvxTableViewCell
    {
        #region Constructors

        protected TaskItemsCellView(IntPtr handle) : base(handle)
        {
            this.DelayBind(() => {
                var set = this.CreateBindingSet<TaskItemsCellView, TaskItem>();
                set.Bind(taskItemDone).For(v => v.Hidden).To(vm => vm.IsDone).WithConversion("TaskItemDone");
                set.Bind(TaskItemNameLabel).To(v => v.Name);
                set.Apply();
            });
        }

        #endregion

        #region Override

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            taskItemDone.Image = taskItemDone.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            taskItemDone.TintColor = new UIColor(red: 0.19f, green: 0.60f, blue: 0.86f, alpha: 1.0f);
        }

        #endregion
    }
}