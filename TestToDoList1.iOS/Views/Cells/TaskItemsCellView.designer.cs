// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TestToDoList1.iOS.Views.Cells
{
    [Register ("TaskItemsCellView")]
    partial class TaskItemsCellView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView taskItemDone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TaskItemNameLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (taskItemDone != null) {
                taskItemDone.Dispose ();
                taskItemDone = null;
            }

            if (TaskItemNameLabel != null) {
                TaskItemNameLabel.Dispose ();
                TaskItemNameLabel = null;
            }
        }
    }
}