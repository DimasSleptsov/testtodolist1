// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [Register ("TaskItemAddView")]
    partial class TaskItemAddView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AddSaveButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView AddTaskItemDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch AddTaskItemDone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField AddTaskItemName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AddSaveButton != null) {
                AddSaveButton.Dispose ();
                AddSaveButton = null;
            }

            if (AddTaskItemDescription != null) {
                AddTaskItemDescription.Dispose ();
                AddTaskItemDescription = null;
            }

            if (AddTaskItemDone != null) {
                AddTaskItemDone.Dispose ();
                AddTaskItemDone = null;
            }

            if (AddTaskItemName != null) {
                AddTaskItemName.Dispose ();
                AddTaskItemName = null;
            }
        }
    }
}