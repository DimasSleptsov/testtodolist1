using MvvmCross.Platforms.Ios.Views;
using System;
using TestToDoList1.Core.ViewModels;

namespace TestToDoList1.iOS.Views
{
    [MvxFromStoryboard("Main")]
    public partial class MainView : MvxViewController<MainViewModel>
    {
        #region Constructors

        protected MainView(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Lifecycles
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewModel.ShowViewPagerCommand.Execute();
            ViewModel.CloseMainViewCommand.Execute();
        }
        #endregion
    }
}
