using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using System;
using TestToDoList1.Core.ViewModels;
using UIKit;


namespace TestToDoList1.iOS.Views
{
    [MvxChildPresentation]
    [MvxFromStoryboard("Main")]
    public partial class TaskDescriptionView : MvxViewController<TaskDescriptionViewModel>
    {
        #region Constructors

        public TaskDescriptionView(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Lifecycles

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            SetupBorder(SaveButton);
            SetupBorder(DeleteButton);
            SetupBorder(shareFacebookButton);
            SetupBorder(TaskItemDescription);

            UITapGestureRecognizer tapGestureRecognizer = new UITapGestureRecognizer(() => View.EndEditing(true))
            {
                CancelsTouchesInView = false
            };
            View.AddGestureRecognizer(tapGestureRecognizer);

            var set = this.CreateBindingSet<TaskDescriptionView, TaskDescriptionViewModel>();
            set.Bind(this).For(v => v.Title).To(vm => vm.Title);
            set.Bind(TaskItemName).To(v => v.Name);
            set.Bind(TaskItemDescription).To(v => v.Description);
            set.Bind(TaskItemDone).To(v => v.IsDone);
            set.Bind(SaveButton).To(v => v.UpdateEntityCommand);
            set.Bind(shareFacebookButton).To(v => v.ShareFacebookCommand);
            set.Bind(DeleteButton).To(v => v.DeleteEntityCommand);
            set.Apply();
        }

        #endregion

        #region Methods

        public void SetupBorder(UIView button)
        {
            button.Layer.BorderWidth = 1;
            button.Layer.CornerRadius = 4;
            button.Layer.BorderColor = UIColor.Gray.CGColor;
        }

        #endregion
    }
}