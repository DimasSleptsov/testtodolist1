using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using System;
using TestToDoList1.Core.ViewModels;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [MvxChildPresentation]
    [MvxFromStoryboard("Main")]
    public partial class TaskItemAddView : MvxViewController<TaskItemAddViewModel>
    {
        #region Constructors

        public TaskItemAddView(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Lifecycles

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            AddSaveButton.Layer.BorderWidth = 1;
            AddSaveButton.Layer.CornerRadius = 4;
            AddSaveButton.Layer.BorderColor = UIColor.Gray.CGColor;

            AddTaskItemDescription.Layer.BorderWidth = 1;
            AddTaskItemDescription.Layer.CornerRadius = 4;
            AddTaskItemDescription.Layer.BorderColor = UIColor.Gray.CGColor;

            AddTaskItemName.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };

            UITapGestureRecognizer tapGestureRecognizer = new UITapGestureRecognizer(() => View.EndEditing(true))
            {
                CancelsTouchesInView = false
            };

            View.AddGestureRecognizer(tapGestureRecognizer);

            var set = this.CreateBindingSet<TaskItemAddView, TaskItemAddViewModel>();
            set.Bind(AddTaskItemName).To(v => v.Name);
            set.Bind(AddTaskItemDescription).To(v => v.Description);
            set.Bind(AddTaskItemDone).To(v => v.IsDone);
            set.Bind(AddSaveButton).To(v => v.AddEntityCommand);
            set.Apply();
        }

        #endregion
    }
}