using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using System;
using TestToDoList1.Core.ViewModels;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [MvxTabPresentation(WrapInNavigationController = true, TabName = "User Information", TabIconName ="UserIcon")]
    [MvxFromStoryboard("Main")]
    public partial class UserDescriptionView : MvxViewController<UserDescriptionViewModel>
    {
        #region Constructors

        public UserDescriptionView(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Lifecycles

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            logOutButton.Layer.BorderWidth = 1;
            logOutButton.Layer.CornerRadius = 4;
            logOutButton.Layer.BorderColor = UIColor.Gray.CGColor;

            UINavigationBar navigationBar = NavigationController.NavigationBar;
            navigationBar.TintColor = UIColor.White;
            navigationBar.BarTintColor = new UIColor(red: 0.35f, green: 0.53f, blue: 0.13f, alpha: 1.0f);
            navigationBar.TitleTextAttributes = new UIStringAttributes
            {
                ForegroundColor = UIColor.White,
                Font = UIFont.FromName("SPSL SwordsmanC", 20)
            };


            var set = this.CreateBindingSet<UserDescriptionView, UserDescriptionViewModel>();
            set.Bind(userName).To(v => v.Name);
            set.Bind(userEmail).To(v => v.Email);
            set.Bind(logOutButton).To(v => v.LogOutUserCommand);
            set.Bind(userImage).For(v => v.ImagePath).To(vm => vm.PictureURL);
            set.Apply();
        }

        #endregion
    }
}