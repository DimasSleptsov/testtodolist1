using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using System;
using TestToDoList1.Core.ViewModels;

namespace TestToDoList1.iOS.Views
{
    [MvxFromStoryboard("Main")]
    [MvxRootPresentation]
    public partial class TabView : MvxTabBarViewController<ViewPagerViewModel>
    {
        #region Variables

        private bool _firstTimePresented = true;

        #endregion

        #region Constructors

        protected TabView(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Lifecycles

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if (_firstTimePresented)
            {
                _firstTimePresented = false;
                ViewModel.ShowTaskItemsCommand.Execute(null);
                ViewModel.ShowUserDescriptionCommand.Execute(null);
            }
        }

        #endregion
    }
}
