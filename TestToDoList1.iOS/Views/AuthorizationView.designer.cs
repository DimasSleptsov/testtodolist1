// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [Register ("AuthorizationView")]
    partial class AuthorizationView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton facebookLoginButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView toDoImage { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (facebookLoginButton != null) {
                facebookLoginButton.Dispose ();
                facebookLoginButton = null;
            }

            if (toDoImage != null) {
                toDoImage.Dispose ();
                toDoImage = null;
            }
        }
    }
}