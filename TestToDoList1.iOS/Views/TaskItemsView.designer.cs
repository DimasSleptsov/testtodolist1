// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [Register ("TaskItemsView")]
    partial class TaskItemsView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tableTaskItems { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (tableTaskItems != null) {
                tableTaskItems.Dispose ();
                tableTaskItems = null;
            }
        }
    }
}