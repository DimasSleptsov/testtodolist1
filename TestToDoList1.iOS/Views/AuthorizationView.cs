using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using System;
using TestToDoList1.Core.ViewModels;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [MvxRootPresentation]
    [MvxFromStoryboard("Main")]
    public partial class AuthorizationView : MvxViewController<AuthorizationViewModel>
    {
        #region Constructors

        public AuthorizationView(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Lifecycles

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            facebookLoginButton.Layer.BorderWidth = 1;
            facebookLoginButton.Layer.CornerRadius = 4;
            facebookLoginButton.Layer.BorderColor = UIColor.Gray.CGColor;

            var set = this.CreateBindingSet<AuthorizationView, AuthorizationViewModel>();
            set.Bind(facebookLoginButton).To(v => v.FacebookLoginCommand);
            set.Apply();
            //facebookLoginButton.TouchUpInside += FacebookLogin;
        }

        //    public override void ViewDidUnload()
        //    {
        //        base.ViewDidUnload();
        //        facebookLoginButton.TouchUpInside -= FacebookLogin;
        //    }

        #endregion

        //    #region Methods

        //    private void FacebookLogin(object sender, EventArgs e)
        //    {
        //        ViewModel.FacebookLoginCommand.Execute();
        //        var ui = ViewModel.Authenticator.GetUI() as UIViewController;
        //        PresentViewController(ui, true, null);
        //    }

        //    #endregion
    }
}
