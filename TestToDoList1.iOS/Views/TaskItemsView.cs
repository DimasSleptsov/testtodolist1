using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using System;
using TestToDoList1.Core.ViewModels;
using UIKit;

namespace TestToDoList1.iOS.Views
{
    [MvxTabPresentation(WrapInNavigationController = true, TabName = "TaskyDrop", TabIconName ="TaskyDropIcon")]
    [MvxFromStoryboard("Main")]
    public partial class TaskItemsView : MvxTableViewController<TaskItemsViewModel>
    {
        #region Constructors

        public TaskItemsView(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Lifecycles

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UIBarButtonItem addButton = new UIBarButtonItem
            {
                Image = UIImage.FromBundle("AddButtonImage"),
                TintColor = UIColor.White
            };
            UINavigationBar navigationBar = NavigationController.NavigationBar;
            UIBarButtonItem backButton = NavigationController.NavigationItem.BackBarButtonItem;

            NavigationController.TopViewController.NavigationItem.SetRightBarButtonItem(addButton, true);

            navigationBar.TintColor = UIColor.White;
            navigationBar.BarTintColor = new UIColor(red: 0.35f, green: 0.53f, blue: 0.13f, alpha: 1.0f);
            navigationBar.TitleTextAttributes = new UIStringAttributes
            {
                ForegroundColor = UIColor.White,
                Font = UIFont.FromName("SPSL SwordsmanC", 20)
            };

            MvxSimpleTableViewSource source = new MvxSimpleTableViewSource(tableTaskItems, "TaskItemsCell");
            var set = this.CreateBindingSet<TaskItemsView, TaskItemsViewModel>();
            set.Bind(addButton).To(v => v.ShowTaskItemAddCommand);
            set.Bind(source).To(v => v.TaskItems);
            set.Bind(source).For(v => v.SelectionChangedCommand).To(vm => vm.TaskItemSelectedCommand);
            set.Apply();

            tableTaskItems.Source = source;
        }

        #endregion
    }
}