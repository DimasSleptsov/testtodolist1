﻿using Facebook.ShareKit;
using Foundation;
using TestToDoList1.Core.Services;
using UIKit;

namespace TestToDoList1.iOS.Services
{
    public class ShareFacebookService : IShareFacebookService
    {
        public void Share()
        {
            UIWindow window = UIApplication.SharedApplication.KeyWindow;
            UIViewController viewController = window.RootViewController;

            while (viewController.PresentedViewController != null)
            {
                viewController = viewController.PresentedViewController;
            }

            if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl(new NSString("fb://"))))
            {
                UIImage image = UIImage.FromBundle("ToDoImage");

                SharePhotoContent content = new SharePhotoContent();

                SharePhoto photo = SharePhoto.From(image, true);
                content.Photos = new SharePhoto[1] { photo };
                ShareDialog dialog = new ShareDialog
                {
                    FromViewController = viewController
                };
                dialog.SetShareContent(content);
                dialog.Mode = ShareDialogMode.Native;
                dialog.Show();
                return;
            }

            UIAlertController okAlertController = UIAlertController.Create("Error", "Your device have not facebook application!", UIAlertControllerStyle.Alert);
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
            viewController.PresentViewController(okAlertController, true, null);
        }
    }
}