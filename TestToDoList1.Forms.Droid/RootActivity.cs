﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using MvvmCross.Forms.Platforms.Android.Views;
using Android.Content.PM;
using Plugin.CurrentActivity;
using Xamarin.Facebook;

namespace TestToDoList1.Forms.Droid
{
    [Activity(
        Label = "TestToDoList1.Forms",
        //Icon = "@mipmap/icon",
        //Theme = "@style/AppTheme",
        MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        LaunchMode = LaunchMode.SingleTask)]
    public class RootActivity : MvxFormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            CrossCurrentActivity.Current.Init(this, bundle);
            FacebookSdk.SdkInitialize(ApplicationContext);
            TabLayoutResource = Resource.Layout.tabbar;
            ToolbarResource = Resource.Layout.toolbar;

            base.OnCreate(bundle);

            //AnimationViewRenderer.Init();
            //PlotViewRenderer.Init();
            //CachedImageRenderer.Init(true);
            //UserDialogs.Init(this);
        }
    }
}