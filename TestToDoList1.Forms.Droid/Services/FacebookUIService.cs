﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.CurrentActivity;
using TestToDoList1.Core.Services;
using Xamarin.Auth;

namespace TestToDoList1.Forms.Droid.Services
{
    class FacebookUIService : IFacebookUIService
    {
        public void GetFacebookUI(OAuth2Authenticator auth)
        {
            Activity currentActivity = CrossCurrentActivity.Current.Activity;
            var ui = auth.GetUI(currentActivity);
            currentActivity.StartActivity(ui);
        }
    }
}