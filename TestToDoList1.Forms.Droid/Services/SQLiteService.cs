﻿using System;
using System.IO;
using TestToDoList1.Core;
using TestToDoList1.Core.Services;

namespace TestToDoList1.Forms.Droid.Services
{
    public class SQLiteService : ISQLiteService
    {
        public string GetName()
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentsPath, App.DataBaseName);

            return path;
        }

    }
}