﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Forms.Platforms.Android.Core;
using MvvmCross.ViewModels;
using TestToDoList1.Core;
using TestToDoList1.Core.Services;
using TestToDoList1.Forms.Droid.Services;
using TestToDoList1.Forms.UI;

namespace TestToDoList1.Forms.Droid
{
    public class Setup : MvxFormsAndroidSetup<App,FormsApp>
    {
        protected override IMvxApplication CreateApp()
        {
            Mvx.IoCProvider.RegisterSingleton<ISQLiteService>(new SQLiteService());
            Mvx.IoCProvider.RegisterSingleton<IFacebookUIService>(new FacebookUIService());
            Mvx.IoCProvider.RegisterSingleton<IShareFacebookService>(new ShareFacebookService());
            return base.CreateApp();
        }
    }
}