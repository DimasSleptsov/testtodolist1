﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;
using Plugin.CurrentActivity;
using TestToDoList1.Core.ViewModels;

namespace TestToDoList1.Droid.Views
{
    [Activity(Label = "@string/app_name", LaunchMode = LaunchMode.SingleTop, ScreenOrientation = ScreenOrientation.Portrait)]
    public class AuthorizationView : MvxActivity<AuthorizationViewModel>
    {
        #region Lifecycles

        protected override void OnCreate(Bundle bundle)
        {
            CrossCurrentActivity.Current.Init(this, bundle);
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.authorization_view);

            Button facebookLoginButton = FindViewById<Button>(Resource.Id.login_button);

            //facebookLoginButton.Click += FacebookLogin;
        }

        #endregion

        #region Methods

        private void FacebookLogin(object sender, EventArgs e)
        {
            ViewModel.FacebookLoginCommand.Execute();
            var ui = ViewModel.Authenticator.GetUI(this);
            StartActivity(ui);
        }

        #endregion
    }
}
