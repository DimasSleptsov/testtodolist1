﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using TestToDoList1.Core.ViewModels;

namespace TestToDoList1.Droid.Views.Fragments
{
    [Register(nameof(TaskItemsView))]
    public class TaskItemsView : BaseFragment<TaskItemsViewModel>
    {
        #region Variables

        private ImageButton _addButton;
        private TextView _title;

        #endregion

        #region Lifecycles

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = base.OnCreateView(inflater, container, savedInstanceState);
            _addButton = view.FindViewById<ImageButton>(Resource.Id.add_button);

            _title = view.FindViewById<TextView>(Resource.Id.title);
            _title.Text = "Tasky Drop";
            _addButton.Click += ShowAddTask;
            _addButton.Visibility = ViewStates.Visible;
            return view;
        }

        #endregion

        #region Override

        protected override int FragmentId => Resource.Layout.task_items_view;

        #endregion

        #region Methods

        public void ShowAddTask(object sender, EventArgs e)
        {
            ViewModel.ShowTaskItemAddCommand.Execute();
        }

        #endregion
    }
}