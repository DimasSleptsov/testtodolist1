﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TestToDoList1.Core.ViewModels;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using System.Collections.Generic;
using Xamarin.Facebook.Share.Widget;
using Xamarin.Facebook;
using ToolbarV7 = Android.Support.V7.Widget.Toolbar;

namespace TestToDoList1.Droid.Views.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame,addToBackStack:true)]
    [Register(nameof(TaskDescriptionView))]
    public class TaskDescriptionView : BaseFragment<TaskDescriptionViewModel>
    {
        #region Variables

        private ImageButton _addButton;
        private TextView _title;
        private EditText _taskItemDescriptionEditText;

        #endregion

        #region Properties

        public List<EditText> EditTexts => (Activity as MainView).EditTexts;

        #endregion

        #region Lifecycles

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            FacebookSdk.SdkInitialize(Context);
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            ToolbarV7 toolbar = view.FindViewById<ToolbarV7>(Resource.Id.toolbar);

            (Activity as MainView).SetSupportActionBar(toolbar);
            (Activity as MainView).SupportActionBar.Title = null;
            (Activity as MainView).SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            _addButton = view.FindViewById<ImageButton>(Resource.Id.add_button);
            _title = view.FindViewById<TextView>(Resource.Id.title);

            _addButton.Visibility = ViewStates.Gone;
            _title.Text = ViewModel.Title;

            _taskItemDescriptionEditText = view.FindViewById<EditText>(Resource.Id.edit_text_description);
            EditTexts.Add(_taskItemDescriptionEditText);

            var shareButton = view.FindViewById<ShareButton>(Resource.Id.share);

            return view;
        }

        #endregion

        #region Override

        protected override int FragmentId => Resource.Layout.task_description_view;

        #endregion
    }
}