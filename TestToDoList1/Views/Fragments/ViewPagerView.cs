﻿using System.Collections.Generic;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using TestToDoList1.Core.ViewModels;
using TestToDoList1.ViewPagerAdapters;
using ViewPager = Android.Support.V4.View.ViewPager;


namespace TestToDoList1.Droid.Views.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame)]
    [Register(nameof(ViewPagerView))]
    public class ViewPagerView : BaseFragment<ViewPagerViewModel>
    {
        #region Lifecycles

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = base.OnCreateView(inflater, container, savedInstanceState);

            ViewPager viewPager = view.FindViewById<ViewPager>(Resource.Id.viewpager);
            if (viewPager != null)
            {
                var fragments = new List<ViewPagerFragmentAdapter.FragmentInfo>
                {
                    new ViewPagerFragmentAdapter.FragmentInfo
                    {
                        FragmentType = typeof(TaskItemsView),
                        Title = "TaskList",
                        ViewModel = ViewModel.TaskItemsViewModel
                    },
                    new ViewPagerFragmentAdapter.FragmentInfo
                    {
                        FragmentType = typeof(UserDescriptionView),
                        Title = "User Information",
                        ViewModel = ViewModel.UserDescriptionViewModel
                    }
                };
                viewPager.Adapter = new ViewPagerFragmentAdapter(Activity, ChildFragmentManager, fragments);
            }

            TabLayout tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabs);
            tabLayout.SetupWithViewPager(viewPager);
            return view;
        }

        #endregion

        #region Override

        protected override int FragmentId => Resource.Layout.view_pager_view;

        #endregion
    }
}