﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TestToDoList1.Core.ViewModels;
using ToolbarV7 = Android.Support.V7.Widget.Toolbar;


namespace TestToDoList1.Droid.Views.Fragments
{
    [Register(nameof(UserDescriptionView))]
    public class UserDescriptionView : BaseFragment<UserDescriptionViewModel>
    {
        #region Variables

        private ToolbarV7 Toolbar;

        #endregion

        #region Lifecycles

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = base.OnCreateView(inflater, container, savedInstanceState);
            Toolbar = view.FindViewById<ToolbarV7>(Resource.Id.toolbar);

            ImageButton addButton = view.FindViewById<ImageButton>(Resource.Id.add_button);
            TextView title = view.FindViewById<TextView>(Resource.Id.title);
            addButton.Visibility = ViewStates.Gone;
            title.Text = "User Information";

            return view;
        }

        #endregion

        #region Override

        protected override int FragmentId => Resource.Layout.user_description_view;

        #endregion
    }
}