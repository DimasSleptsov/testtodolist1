﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TestToDoList1.Core.ViewModels;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using System.Collections.Generic;
using ToolbarV7 = Android.Support.V7.Widget.Toolbar;

namespace TestToDoList1.Droid.Views.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame, addToBackStack: true)]
    [Register(nameof(TaskItemAddView))]
    public class TaskItemAddView : BaseFragment<TaskItemAddViewModel>
    {
        #region Variables

        private ImageButton _addButton;
        private TextView _title;

        #endregion

        #region Properties

        public List<EditText> EditTexts => (Activity as MainView).EditTexts;

        #endregion

        #region Lifecycles

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = base.OnCreateView(inflater, container, savedInstanceState);
            ToolbarV7 toolbar = view.FindViewById<ToolbarV7>(Resource.Id.toolbar);
            _addButton = view.FindViewById<ImageButton>(Resource.Id.add_button);
            _title = view.FindViewById<TextView>(Resource.Id.title);
            _addButton.Visibility = ViewStates.Gone;
            _title.Text = "Add Task";

            (Activity as MainView).SetSupportActionBar(toolbar);
            (Activity as MainView).SupportActionBar.Title = null;
            (Activity as MainView).SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            EditText editTextName = view.FindViewById<EditText>(Resource.Id.add_edit_text_name);
            EditTexts.Add(editTextName);
            EditText editTextDescription = view.FindViewById<EditText>(Resource.Id.add_edit_text_description);
            EditTexts.Add(editTextDescription);

            return view;
        }

        #endregion

        #region Override

        protected override int FragmentId => Resource.Layout.task_item_add_view;

        #endregion
    }
}