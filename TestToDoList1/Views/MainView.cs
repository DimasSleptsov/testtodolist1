﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using TestToDoList1.Core.ViewModels;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Droid.Support.V7.AppCompat;
using TestToDoList1.Droid.Views.Fragments;
using MvvmCross.Commands;
using Android.Views.InputMethods;
using System.Collections.Generic;
using System;
using Android.Content.PM;
using Plugin.CurrentActivity;

namespace TestToDoList1.Droid.Views
{
    [MvxActivityPresentation]
    [Activity(Label = "@string/app_name", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainView : MvxAppCompatActivity<MainViewModel>
    {

        #region Properties

        public List<EditText> EditTexts { get; set; }

        #endregion

        #region Lifecycles

        protected override void OnCreate(Bundle bundle)
        {
            CrossCurrentActivity.Current.Init(this, bundle);
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.main_view);
            Transaction(nameof(TaskItemsView), ViewModel.ShowViewPagerCommand);

            EditTexts = new List<EditText>();
        }

        #endregion

        #region Override

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.TitleFormatted == null)
            {
                OnBackPressed();
            }
            return base.OnOptionsItemSelected(item);
        }

        public override bool DispatchTouchEvent(MotionEvent ev)
        {
            bool result = base.DispatchTouchEvent(ev);

            if (EditTexts == null || CurrentFocus == null || !(CurrentFocus is EditText))
            {
                return result;
            }

            bool hide = true;
            foreach (EditText editText in EditTexts)
            {
                if (IsPointInsideView(ev.RawX, ev.RawY, editText))
                {
                    hide = false;
                }
            }

            if (hide)
            {
                HideSoftKeyboard();
            }
            return result;
        }

        #endregion

        #region Methods

        public void Transaction(string tag, IMvxCommand navigationCommand)
        {
            var fragment = SupportFragmentManager.FindFragmentByTag(tag);
            if (fragment != null && fragment.IsAdded != true)
            {
                var transaction = SupportFragmentManager.BeginTransaction();
                transaction.Replace(Resource.Id.content_frame, fragment);
                transaction.Commit();
                return;
            }

            navigationCommand.Execute(null);
        }

        public void HideSoftKeyboard()
        {
            InputMethodManager inputMethodManager =
                (InputMethodManager)GetSystemService(
                    InputMethodService);

            if (CurrentFocus != null)
            {
                inputMethodManager.HideSoftInputFromWindow(
                    CurrentFocus.WindowToken, 0);
            }
        }

        private bool IsPointInsideView(float x, float y, View view)
        {
            int[] location = new int[2];
            view.GetLocationOnScreen(location);
            int viewX = location[0];
            int viewY = location[1];

            if ((x > viewX && x < (viewX + view.Width)) &&
                (y > viewY && y < (viewY + view.Height)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HideKeyboardToolbar(Object sender, EventArgs e)
        {
            if (EditTexts == null || CurrentFocus == null || !(CurrentFocus is EditText))
            {
                return;
            }
            HideSoftKeyboard();
        }
        #endregion

    }
}