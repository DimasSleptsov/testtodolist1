﻿using System.Collections.Generic;
using System.Reflection;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using MvvmCross;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Presenters;
using MvvmCross.ViewModels;
using TestToDoList1.Core;
using TestToDoList1.Core.Services;
using TestToDoList1.Droid.Services;
using Xamarin.Facebook.Share.Widget;

namespace TestToDoList1.Droid
{
    public class Setup : MvxAppCompatSetup<App>
    {
        protected override IEnumerable<Assembly> AndroidViewAssemblies => new List<Assembly>(base.AndroidViewAssemblies)
        {
            typeof(NavigationView).Assembly,
            typeof(FloatingActionButton).Assembly,
            typeof(Toolbar).Assembly,
            typeof(DrawerLayout).Assembly,
            typeof(MvxRecyclerView).Assembly,
            typeof(MvxSwipeRefreshLayout).Assembly,
            typeof(ShareButton).Assembly
        };

        protected override IMvxApplication CreateApp()
        {
            Mvx.IoCProvider.RegisterSingleton<ISQLiteService>(new SQLiteService());
            Mvx.IoCProvider.RegisterSingleton<IShareFacebookService>(new ShareFacebookService());
            Mvx.IoCProvider.RegisterSingleton<IFacebookUIService>(new FacebookUIService());

            return base.CreateApp();
        }

        protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry)
        {
            MvxAppCompatSetupHelper.FillTargetFactories(registry);
            base.FillTargetFactories(registry);
        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            return new MvxAppCompatViewPresenter(AndroidViewAssemblies);
        }
    }
}
