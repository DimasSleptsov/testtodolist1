﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace TestToDoList1.Droid.Converters
{
    public class TaskItemDoneValueConverter: MvxValueConverter<bool, string>
    {
        protected override string Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ? "Visible" : "Gone";
        }
    }
}