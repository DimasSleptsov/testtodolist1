﻿using Android.App;
using Android.Content.PM;
using Android.Graphics;
using Android.Runtime;
using Plugin.CurrentActivity;
using TestToDoList1.Core.Services;
using Xamarin.Facebook.Share.Model;
using Xamarin.Facebook.Share.Widget;

namespace TestToDoList1.Droid.Services
{
    public class ShareFacebookService : IShareFacebookService
    {
        public void Share()
        {
            Activity currentActivity = CrossCurrentActivity.Current.Activity;

            if (IsAppInstalled("com.facebook.katana",currentActivity))
            {
                Bitmap bitmap = BitmapFactory.DecodeResource(currentActivity.Resources, Resource.Drawable.todo_manager_icon);
                SharePhoto.Builder builder = new SharePhoto.Builder();
                builder.SetBitmap(bitmap);

                SharePhoto photo = builder.Build().JavaCast<SharePhoto>();
                SharePhotoContent.Builder contentBuilder = new SharePhotoContent.Builder();
                contentBuilder.AddPhoto(photo);

                SharePhotoContent content = contentBuilder.Build();

                ShareDialog shareDialog = new ShareDialog(currentActivity);
                shareDialog.Show(content);
                return;
            }

            AlertDialog.Builder dialog = new AlertDialog.Builder(currentActivity);
            AlertDialog alert = dialog.Create();
            alert.SetMessage("Your device have not facebook application!");
            alert.SetButton("OK", (c, ev) => { });
            alert.Show();
        }

        private bool IsAppInstalled(string packageName, Activity activity)
        {
            PackageManager packageManager = activity.PackageManager;
            bool installed = false;
            try
            {
                packageManager.GetPackageInfo(packageName, PackageInfoFlags.Activities);
                installed = true;
            }
            catch (PackageManager.NameNotFoundException)
            {
                installed = false;
            }
            return installed;
        }
    }
}