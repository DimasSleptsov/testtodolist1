﻿using System;
using System.Collections.Generic;
using Android.Support.V4.App;
using Android.Runtime;
using Android.Content;
using System.Linq;
using MvvmCross.Droid.Support.V4;
using Java.Lang;
using MvvmCross.ViewModels;

namespace TestToDoList1.ViewPagerAdapters
{
    public class ViewPagerFragmentAdapter : FragmentStatePagerAdapter
    {
        private readonly Context _context;

        public IEnumerable<FragmentInfo> Fragments { get; private set; }

        public override int Count
        {
            get { return Fragments.Count(); }
        }

        protected ViewPagerFragmentAdapter(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }

        public ViewPagerFragmentAdapter(
            Context context, FragmentManager fragmentManager, IEnumerable<FragmentInfo> fragments)
            : base(fragmentManager)
        {
            _context = context;
            Fragments = fragments;
        }

        public override Fragment GetItem(int position)
        {
            FragmentInfo fragmentInfo = Fragments.ElementAt(position);
            Fragment fragment = Fragment.Instantiate(_context, FragmentJavaName(fragmentInfo.FragmentType));
            ((MvxFragment)fragment).ViewModel = fragmentInfo.ViewModel;
            return fragment;
        }

        protected static string FragmentJavaName(Type fragmentType)
        {
            return Java.Lang.Class.FromType(fragmentType).Name;
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {
            return new Java.Lang.String(Fragments.ElementAt(position).Title);
        }

        public class FragmentInfo
        {
            public string Title { get; set; }

            public Type FragmentType { get; set; }

            public IMvxViewModel ViewModel { get; set; }
        }
    }
}