﻿using MvvmCross.Platforms.Ios.Core;
using TestToDoList1.Core;
using MvvmCross.ViewModels;
using MvvmCross;
using TestToDoList1.Core.Services;
using TestToDoList1.Forms.iOS.Services;
using TestToDoList1.Forms.UI;
using MvvmCross.Forms.Platforms.Ios.Core;

namespace TestToDoList1.Forms.iOS
{
    public class Setup : MvxFormsIosSetup<App, FormsApp>
    {
        protected override IMvxApplication CreateApp()
        {
            Mvx.IoCProvider.RegisterSingleton<ISQLiteService>(new SQLiteService());
            Mvx.IoCProvider.RegisterSingleton<IShareFacebookService>(new ShareFacebookService());
            Mvx.IoCProvider.RegisterSingleton<IFacebookUIService>(new FacebookUIService());
            return new App();
        }

    }
}