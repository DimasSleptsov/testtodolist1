﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using TestToDoList1.Core.Services;
using UIKit;
using Xamarin.Auth;

namespace TestToDoList1.Forms.iOS.Services
{
    public class FacebookUIService : IFacebookUIService
    {
        public void GetFacebookUI(OAuth2Authenticator auth)
        {
            UIWindow window = UIApplication.SharedApplication.KeyWindow;
            UIViewController viewController = window.RootViewController;

            while (viewController.PresentedViewController != null)
            {
                viewController = viewController.PresentedViewController;
            }

            var ui = auth.GetUI() as UIViewController;
            viewController.PresentViewController(ui, true, null);
        }
    }
}