﻿using Foundation;
using MvvmCross.Forms.Platforms.Ios.Core;
using TestToDoList1.Core;
using TestToDoList1.Forms.iOS;
using TestToDoList1.Forms.UI;
using UIKit;

namespace TestToDoList1.Forms.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : MvxFormsApplicationDelegate<Setup, App, FormsApp>
    {
        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            base.FinishedLaunching(application, launchOptions);
            return true;
        }

        protected override void RunAppStart(object hint = null)
        {
            base.RunAppStart(hint);
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init();
        }
    }
}


